package fr.ensicaen.demonetik.domain.scenario;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ensicaen.demonetik.dao.ActorDao;
import fr.ensicaen.demonetik.domain.Provider;
import fr.ensicaen.demonetik.domain.actor.ActorLoader;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ScenarioLoader implements Provider<Scenario> {

    private static Logger logger = LoggerFactory.getLogger(ScenarioLoader.class);

    @Override
    public List<Scenario> provideElements() {

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<Scenario> scenarios = new ArrayList<>();

        //Pre-loading actors
        ActorDao.getInstance().peopling(new ActorLoader());

        try {

            File folder = new ClassPathResource("scenarios").getFile();

            File[] files = folder.listFiles(
                    (File dir, String name) -> name.endsWith("json")
            );

            for (File file : files) {

                Scenario s = objectMapper.readValue(file, Scenario.class);

                if (actorVerification(s)) {
                    s.setId(UUID.randomUUID().toString());
                    sortSteps(s);
                    scenarios.add(s);
                    logger.info("[Scenario] " + s.getName() + " loaded (" + file.getName() + ")");
                } else {
                    logger.error("Impossible to load " + s.getName() + " scenario");
                }

            }

        } catch (IOException e) {
            logger.error("Scenario loading error ", e);
        }

        return scenarios;
    }

    private void sortSteps(Scenario s) {

        List<Step> unsorted = s.getSteps();
        List<Step> sorted = unsorted.stream().sorted(

                (s1, s2) -> {
                    if (s1.getSequence() > s2.getSequence()) {
                        return 1;
                    } else if (s1.getSequence() < s2.getSequence()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }).collect(Collectors.toList());

        s.getSteps().clear();
        s.getSteps().addAll(sorted);
    }

    private static boolean actorVerification(Scenario scenario) {

        List<Step> steps = scenario.getSteps();

        List<String> actorsDeclared = scenario.getActors();

        List<String> actors = steps.stream()
                .map(Step::getActor)
                .distinct()
                .collect(Collectors.toList());

        for (String actor : actorsDeclared) {
            if (!ActorDao.getActors().containsKey(actor)) {
                logger.error("Scenario : {} - The next actor is missing in cache : {}", scenario.getName(), actor);
                return false;
            }
        }

        for (String actor : actors) {
            if (!actorsDeclared.contains(actor)) {
                logger.error("Scenario : {} - \"{}\" is not declared in 'actors' list", scenario.getName(), actor);
                return false;
            }
        }

        return true;
    }


}
