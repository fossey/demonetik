package fr.ensicaen.demonetik.domain.scenario;

import fr.ensicaen.demonetik.api.error.DemonetikException;
import fr.ensicaen.demonetik.dao.ScenarioDao;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.domain.transaction.TransactionService;
import fr.ensicaen.demonetik.model.LoadingResponse;
import fr.ensicaen.demonetik.model.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

@Service
public class ScenarioService {

    private ScenarioDao scenarioDao = ScenarioDao.getInstance();

    @Autowired
    private TransactionService transactionService;

    @Autowired
    public ScenarioService() {
        scenarioDao.peopling(new ScenarioLoader());
    }

    public List<Scenario> getScenarios() {
        return scenarioDao.getScenarios();
    }

    public LoadingResponse loadScenario(String id) throws DemonetikException {

        Scenario scenario = scenarioDao.getScenarioById(id);
        Transaction t = transactionService.create(scenario);

        LoadingResponse loadingResponse = new LoadingResponse();
        loadingResponse.setTransactionId(t.getId());

        return loadingResponse;
    }

    // Workaround to clone deeply scenario instance
    public static Scenario cloneScenario(Scenario scenario) throws DemonetikException {

        Object copy = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(scenario);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            copy = ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new DemonetikException(e);
        }

        return (Scenario) copy;
    }

}
