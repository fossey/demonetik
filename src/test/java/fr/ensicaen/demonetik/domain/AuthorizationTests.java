package fr.ensicaen.demonetik.domain;

import fr.ensicaen.demonetik.domain.steps.Authorization;
import fr.ensicaen.demonetik.domain.steps.Context;
import fr.ensicaen.demonetik.domain.steps.StepDefaultImpl;
import fr.ensicaen.demonetik.domain.transaction.MessageParser;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AuthorizationTests {

    private Context context;

    @Before
    public void setUp(){

        Step s1 = DummyElementFactory.createDummyStep(1, "A1", "HOLDER_IDENTIFICATION", Step.State.HOLDER_IDENTIFICATION, null);
        Step s2 = DummyElementFactory.createDummyStep(2, "A2", "REQUEST_AMOUNT", Step.State.REQUEST_AMOUNT, null);
        Step s3 = DummyElementFactory.createDummyStep(4, "A3", "REQUEST_PIN", Step.State.REQUEST_PIN, null);
        Step s4 = DummyElementFactory.createDummyStep(3, "A4", "TRANSACTION_PROCESSING", Step.State.TRANSACTION_PROCESSING, null);
        Step s5bis = DummyElementFactory.createDummyStep(4, "A4", "TRANSACTION_FAILED", Step.State.TRANSACTION_FAILED, null);
        s4.getScenarioFork().add(s5bis);
        Step s5 = DummyElementFactory.createDummyStep(4, "A5", "TRANSACTION_SUCCEED", Step.State.TRANSACTION_SUCCEED, null);

        Scenario scenario = DummyElementFactory.createDummyScenario("Scenario", "Description", s1, s2, s3, s4, s5);

        Transaction t = new Transaction(scenario, (id,o) -> {});
        context = new Context(t, new MessageParser());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testArgumentsValidation() {
        new Authorization();
    }

    @Test
    public void testExecute() {

        StepDefaultImpl stepDefault = new StepDefaultImpl();
        Authorization authorization = new Authorization("200", "amount");

        stepDefault.execute(context); //HOLDER_IDENTIFICATION
        stepDefault.execute(context); //REQUEST_AMOUNT
        context.getTransaction().putData("amount", "500");
        stepDefault.execute(context); //REQUEST_PIN
        authorization.execute(context); //TRANSACTION_PROCESSING

        assertEquals(Step.State.TRANSACTION_FAILED, context.getTransaction().getState());
    }
}
