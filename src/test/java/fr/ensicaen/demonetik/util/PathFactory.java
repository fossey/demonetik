package fr.ensicaen.demonetik.util;

public class PathFactory {

    /* Scenario path*/

    public static String SCENARIOS_PATH = "/scenarios";
    public static String LOAD_SCENARIO_PATH = "/load";

    public static String getLoadScenario(String id) {

        return SCENARIOS_PATH + "/" + id + LOAD_SCENARIO_PATH;
    }

    public static String getScenarios() {
        return SCENARIOS_PATH;
    }

    /* Transaction path */

    public static String TRANSACTION_PATH = "/transactions/";
    public static String STATE_TRANSACTION_PATH = "/state";

    public static String putTransaction(String id) {
        return TRANSACTION_PATH + id;
    }

    public static String postTransactionData(String id) { return TRANSACTION_PATH + id + "/data"; }
    public static String getTransactionState(String id) {
        return TRANSACTION_PATH + id + STATE_TRANSACTION_PATH;
    }
}
