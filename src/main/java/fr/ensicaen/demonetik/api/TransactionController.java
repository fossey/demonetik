package fr.ensicaen.demonetik.api;

import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.domain.transaction.TransactionService;
import fr.ensicaen.demonetik.model.TransactionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;


    /**
     * Retrieve all transactions created
     *
     * @return All transactions
     */
    @RequestMapping(method = RequestMethod.GET)
    public List<Transaction> getTransactions(){
        return transactionService.getTransactions();
    }

    /**
     * Delete all transactions
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteTransactions(){
        transactionService.deleteTransactions();
    }

    /**
     * Move transaction to next scenario step
     *
     * @param transactionId Transaction id
     * @return New step
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If transaction id is not found
     */
    @RequestMapping(path = "/{transactionId}", method = RequestMethod.PUT)
    public String goToTheNextStep(@PathVariable(value = "transactionId") String transactionId) {
        return transactionService.goToTheNextStep(transactionId).value();
    }

    /**
     * Get transaction
     *
     * @param transactionId Transaction id
     * @return Transacyion
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If transaction id is not found
     */
    @RequestMapping(path = "/{transactionId}", method = RequestMethod.GET)
    public Transaction getTransaction(@PathVariable(value = "transactionId") String transactionId) {
        return transactionService.getTransaction(transactionId);
    }

    /**
     * Post data in transaction context
     *
     * @param transactionId Transaction id
     * @param data Data with
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If transaction id is not found
     */
    @RequestMapping(path = "/{transactionId}/data", method = RequestMethod.POST)
    public void postData(@PathVariable(value = "transactionId") String transactionId, @RequestBody TransactionData data) {
        transactionService.putData(transactionId, data.getKey(), data.getValue());
    }

    /**
     * Retrieve value in transaction context
     *
     * @param transactionId Transaction id
     * @param key Key value
     * @return Variable value as a String
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If transaction id is not found
     */
    @RequestMapping(path = "/{transactionId}/data/{key}", method = RequestMethod.GET)
    public String getData(@PathVariable(value = "transactionId") String transactionId, @PathVariable(value = "key") String key) {
        return transactionService.getData(transactionId, key);
    }

    /**
     * Get current transaction state
     *
     * @param transactionId Transaction id
     * @return New step
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If transaction id is not found
     */
    @RequestMapping(path = "/{transactionId}/state", method = RequestMethod.GET)
    public String getState(@PathVariable(value = "transactionId") String transactionId) {
        return transactionService.getState(transactionId).value();
    }

}
