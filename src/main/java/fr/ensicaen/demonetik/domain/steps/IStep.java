package fr.ensicaen.demonetik.domain.steps;

public interface IStep {

    void execute(Context context) ;

}
