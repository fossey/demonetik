package fr.ensicaen.demonetik.domain.steps;

import fr.ensicaen.demonetik.api.error.DemonetikException;

import java.lang.reflect.Constructor;

@SuppressWarnings("squid:S3878") // new Object[]{args} is needed
public class StepsLoader {

    private StepsLoader() {
        throw new IllegalStateException("Utility class");
    }

    public static IStep loadStep(String qualifiedStepName, String... args) throws DemonetikException {

        Class<?> stepClass;
        Constructor<?> constructor;

        try {

            Class<?> uncheckedStep = Class.forName(qualifiedStepName);
            if (IStep.class.isAssignableFrom(uncheckedStep)) {
                stepClass = Class.class.cast(uncheckedStep);
            } else {
                throw new DemonetikException("Class " + qualifiedStepName + ".java doesn't implements IStep interface");
            }

            constructor = stepClass.getConstructor(args.getClass());
            return IStep.class.cast(constructor.newInstance(new Object[]{args}));

        } catch (Exception e) {
            throw new DemonetikException(e);
        }
    }
}
