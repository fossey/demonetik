const HOST = "localhost";
const PORT = "8080";
const BASE_URL = "/demonetik";
const TRANSACTION_PATH = BASE_URL + "/transactions";
const WEB_SOCKET_PATH = "ws://" + HOST + ":" + PORT + BASE_URL + "/web-socket";

let app = angular.module("demonetik", [])
    .service("TransactionService", function () {

        return {
            id: '',
            actors: [],
            steps: [],
            barStatus: 'progress-bar-animated',
            barWidth: {'width': '0%'}
        }
    })
    .service("WebSocketService", function () {

        let ws = new WebSocket(WEB_SOCKET_PATH);

        ws.onopen = function () {
            console.log("Socket connected");
        };

        ws.onmessage = function (event) {
            console.log("Event : ");
            console.log(event.data);
        };

        ws.onclose = function () {
            console.log("Socket closed");
        };

        ws.onerror = function (event) {
            console.error(event);
        };

        return ws;
    })
    .service("TransactionsService", ["$http", "WebSocketService", "TransactionService", function ($http, WebSocketService, TransactionService) {

        let service = {};
        let websocket = WebSocketService;
        let Transaction = TransactionService;
        service.isEmpty = true;

        service.init = function () {

            websocket.onmessage = function (event) {

                if (event.data === "TRANSACTION-CREATED") {
                    service.getTransactions();
                } else {

                    if (Transaction.id === event.data) {
                        service.getTransaction(event.data);
                    }
                }
            };

        };

        service.deleteAll = function () {
            $http.delete(TRANSACTION_PATH)
                .then(service.getTransactions);
        };

        service.getTransaction = function (id) {
            $http.get(TRANSACTION_PATH + "/" + id)
                .then(function (response) {

                    let t = response.data;
                    Transaction.id = id;
                    Transaction.steps = t.pastSteps;
                    Transaction.actors = t.actors;
                    Transaction.barWidth = {'width': t.progression.percent + '%'};

                    if (t.state === "TRANSACTION_SUCCEED") {
                        Transaction.barStatus = 'bg-success';
                    } else if (t.state === "TRANSACTION_FAILED") {
                        Transaction.barStatus = 'bg-danger';
                    } else {
                        Transaction.barStatus = 'progress-bar-animated';
                    }

                });
        };

        service.getTransactions = function () {
            return $http.get(TRANSACTION_PATH)
                .then(function (response) {

                    service.transactionsId = response.data.map(transaction => transaction.id);
                    service.transactions = response.data;

                    service.isEmpty = service.transactionsId.length === 0;

                    if (!service.isEmpty && Transaction.id !== service.transactionsId[0]) {
                        service.getTransaction(service.transactionsId[0]);
                    }

                });
        };

        return service;
    }])
    .controller("TransactionsCtrl", ["$scope", "$http", "TransactionService", "TransactionsService", function ($scope, $http, TransactionService, TransactionsService) {

        $scope.Transaction = TransactionService;
        $scope.webservice = TransactionsService;

        $scope.webservice.init();
        $scope.webservice.getTransactions();

        $scope.selectFirst = function () {
            $scope.webservice.getTransaction($scope.webservice.transactionsId[0]);
        };

    }]);