package fr.ensicaen.demonetik.domain.transaction;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageParser implements IMessageParser {

    @Override
    public String parseMessage(Map<String, String> sharedMemory, String message) {

        String startVariableDelimiter = "\\{\\{";
        String endVariableDelimiter = "}}";

        Pattern variablePattern = Pattern.compile(startVariableDelimiter + ".*?" + endVariableDelimiter);

        Matcher matcher = variablePattern.matcher(message);

        while (matcher.find()) {

            String variable = matcher.group()
                    .replaceAll(startVariableDelimiter, "")
                    .replaceAll(endVariableDelimiter, "");

            String value = sharedMemory.get(variable);

            if (value != null) {
                message = message.replaceAll(startVariableDelimiter + variable + endVariableDelimiter, value);
            }
        }

        return message;
    }
}
