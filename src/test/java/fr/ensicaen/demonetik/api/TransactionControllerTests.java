package fr.ensicaen.demonetik.api;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.domain.transaction.TransactionService;
import fr.ensicaen.demonetik.util.PathFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService transactionService;

    @Test
    public void testGoToTheNextStep() throws Exception {

        String fakeId = "fake-uuid";

        doThrow(NotFoundException.class).when(transactionService).goToTheNextStep(fakeId);

        this.mockMvc.perform(put(PathFactory.putTransaction(fakeId)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
