package fr.ensicaen.demonetik.domain.websocket;

import fr.ensicaen.demonetik.domain.scenario.IReceiver;
import fr.ensicaen.demonetik.model.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SessionHandler extends TextWebSocketHandler implements IReceiver {

    private static Logger logger = LoggerFactory.getLogger(SessionHandler.class);

    private WebSocketSession session;
    private static SessionHandler instance;

    private SessionHandler() {
    }

    public static SessionHandler getInstance() {
        if (instance == null) {
            instance = new SessionHandler();
        }

        return instance;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        logger.info("Connection established");
        this.session = session;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message)
            throws Exception {
        if ("CLOSE".equalsIgnoreCase(message.getPayload())) {
            session.close();
        } else {
            logger.info("Received:" + message.getPayload());
        }
    }

    public void sendMessage(String message) {

        logger.info("Trying to send : {}", message);

        if (session != null && session.isOpen()) {

            try {
                session.sendMessage(new TextMessage(message));
            } catch (Exception e) {
                logger.error("Message not sent", e);
            }

        } else {
            logger.warn("Don't have open session to send message");
        }
    }

    // This will send only to one client (most recently connected)
    @Override
    public void sendStep(String id, Step step) {
        sendMessage(id);

    }
}
