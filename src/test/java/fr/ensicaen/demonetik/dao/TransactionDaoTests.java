package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.domain.websocket.SessionHandler;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertNotNull;

public class TransactionDaoTests {

    private TransactionDao instance;

    private Scenario dummyScenario;

    @Before
    public void setUp(){
        instance = TransactionDao.getInstance();
        dummyScenario = DummyElementFactory.createDummyScenario();
    }

    @Test(expected = NotFoundException.class)
    public void testTransactionNotFound(){
        instance.getTransactionById("not-found-uuid");
    }

    @Test
    public void testGetTransactionById(){

        Transaction expected = new Transaction(dummyScenario, SessionHandler.getInstance());
        instance.save(expected);
        Transaction tested = instance.getTransactionById(expected.getId());

        assertNotNull(tested);
        //If transaction is not UUID, fromString method will throw exception
        UUID.fromString(tested.getId());

    }
}
