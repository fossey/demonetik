package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import org.junit.Test;

public class ScenarioDaoTests {

    @Test(expected = NotFoundException.class)
    public void testScenarioIsNotFound(){
        ScenarioDao.getInstance().getScenarioById("not-found-uuid");
    }

}
