package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.domain.Provider;
import fr.ensicaen.demonetik.model.Actor;

import java.util.HashMap;
import java.util.Map;

public class ActorDao {


    private static ActorDao instance;
    private static Map<String, Actor> actors = new HashMap<>();

    private ActorDao() {}

    public void peopling(Provider<Actor> actorProvider){
        actorProvider.provideElements().forEach(
                actor -> actors.put(actor.getName(),actor)
        );
    }

    public static ActorDao getInstance() {

        if(instance == null){
            instance = new ActorDao();
        }

        return instance;
    }

    public static Map<String, Actor> getActors() {
        return actors;
    }
}
