package fr.ensicaen.demonetik.domain.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
@EnableScheduling
public class WebSocket implements WebSocketConfigurer {

    public static final String TRANSACTION_CREATED = "TRANSACTION-CREATED";
    public static final String WEB_SOCKET_PATH = "/web-socket";

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(SessionHandler.getInstance(), WEB_SOCKET_PATH);
    }

}