package fr.ensicaen.demonetik.api.error;

public class DemonetikException extends Exception {

    public DemonetikException(Throwable throwable) {
        super(throwable);
    }

    public DemonetikException(String s) {
        super(s);
    }
}