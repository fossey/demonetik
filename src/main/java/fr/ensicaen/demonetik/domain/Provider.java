package fr.ensicaen.demonetik.domain;

import java.util.List;

public interface Provider<E> {

    List<E> provideElements();
}
