package fr.ensicaen.demonetik.api;

import fr.ensicaen.demonetik.api.error.DemonetikException;
import fr.ensicaen.demonetik.domain.scenario.ScenarioService;
import fr.ensicaen.demonetik.model.LoadingResponse;
import fr.ensicaen.demonetik.model.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/scenarios")
public class ScenarioController {

    @Autowired
    private ScenarioService scenarioService;

    /**
     * Retrieve all scenarios loaded in cache
     *
     * @return return all scenarios
     */
    @RequestMapping(method = RequestMethod.GET)
    public List<Scenario> getScenarios() {
        return scenarioService.getScenarios();
    }


    /**
     * Load specified scenario
     *
     * @param id Scenario id
     * @return Loading response containing transaction id initialized from this scenario
     * @throws fr.ensicaen.demonetik.api.error.NotFoundException If scenario id is not found
     */
    @RequestMapping(path = "/{id}/load", method = RequestMethod.GET)
    public LoadingResponse loadScenario(@PathVariable(value = "id") String id) throws DemonetikException {
        return scenarioService.loadScenario(id);
    }
}
