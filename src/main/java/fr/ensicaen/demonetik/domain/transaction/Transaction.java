package fr.ensicaen.demonetik.domain.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.dao.ActorDao;
import fr.ensicaen.demonetik.domain.scenario.IReceiver;
import fr.ensicaen.demonetik.model.Actor;
import fr.ensicaen.demonetik.model.Progression;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.model.Step.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Transaction {

    private static Logger logger = LoggerFactory.getLogger(Transaction.class);

    private UUID id;
    private HashMap<String, String> sharedMemory = new HashMap<>();
    private Deque<Step> pastSteps = new LinkedList<>();
    private String name;
    private LocalTime creation;
    private int currentStep;

    @JsonIgnore
    private IReceiver observer;

    @JsonIgnore
    private Scenario scenario;

    public Transaction(Scenario s, IReceiver o) {
        id = UUID.randomUUID();
        scenario = s;
        observer = o;
        currentStep = 0;
        name = s.getName();
        creation = LocalTime.now();

        logger.info("Transaction initialized.");
        logger.info("State : " + getCurrentStep().getState());
    }

    public String getId() {
        return id.toString();
    }

    public State getState() {
        return getCurrentStep().getState();
    }

    public void incrementCurrentStep() {
        currentStep++;
    }

    public Deque<Step> getPastSteps() {
        return pastSteps;
    }

    public String getName() {
        return name;
    }

    @JsonIgnore
    public Step getCurrentStep() {
        return scenario.getSteps().get(currentStep);
    }

    @JsonIgnore
    public int getCurrentStepValue() {
        return currentStep;
    }

    public void putData(String key, String value) {
        sharedMemory.put(key, value);
    }

    public Scenario getScenario() {
        return scenario;
    }

    public Map<String, String> getSharedMemory() {
        return sharedMemory;
    }

    public String getData(String key) {
        String value = sharedMemory.get(key);

        if (value == null) {
            String errorMessage = "Variable [" + key + "] is not found in transaction " + getId();
            NotFoundException e = new NotFoundException(errorMessage);
            logger.error(errorMessage);
            throw e;
        }

        return value;
    }

    public List<Actor> getActors() {
        List<Actor> actors = new ArrayList<>();

        scenario.getActors().forEach(actorName -> actors.add(ActorDao.getActors().get(actorName)));

        return actors;
    }

    @JsonIgnore
    public LocalTime getDateObject(){
        return creation;
    }

    public String getDate() {
        return creation.format(DateTimeFormatter.ISO_LOCAL_TIME);
    }

    public Progression getProgression() {

        Progression progress = new Progression();

        double total = scenario.getSteps().size();
        double current = currentStep + 1.0;

        progress.setPercent((current / total) * 100);

        return progress;
    }

    public void sendStep(Step step) {
        observer.sendStep(getId(), step);
    }
}
