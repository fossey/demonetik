package fr.ensicaen.demonetik.api;

import fr.ensicaen.demonetik.domain.scenario.ScenarioService;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ScenarioController.class)
public class ScenarioControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScenarioService scenarioService;

    private Scenario scenario;
    private Step step;

    @Before
    public void setUp() {
        this.scenario = DummyElementFactory.createDummyScenario();
        this.scenario.getSteps().get(0).setStepImpl(DummyElementFactory.createDummyStepImpl("fr.some.Class",new String[]{"arg1"}));
        this.step = scenario.getSteps().get(0);
    }

    @Test
    public void testGetScenariosEmpty() throws Exception {

        given(this.scenarioService.getScenarios()).willReturn(Arrays.asList());

        this.mockMvc.perform(get("/scenarios").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));

    }

    @Test
    public void testGetScenario() throws Exception {

        given(this.scenarioService.getScenarios()).willReturn(Arrays.asList(scenario));

        this.mockMvc.perform(get("/scenarios").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(scenario.getName())))
                .andExpect(jsonPath("$[0].description", is(scenario.getDescription())))
                .andExpect(jsonPath("$[0].steps", hasSize(scenario.getSteps().size())))
                .andExpect(jsonPath("$[0].steps[0].actor", is(step.getActor())))
                .andExpect(jsonPath("$[0].steps[0].sequence", is(step.getSequence())))
                .andExpect(jsonPath("$[0].steps[0].state", is(step.getState().value())))
                .andExpect(jsonPath("$[0].steps[0].message", is(step.getMessage())))
                .andExpect(jsonPath("$[0].steps[0].stepImpl.className", is(step.getStepImpl().getClassName())))
                .andExpect(jsonPath("$[0].steps[0].stepImpl.arguments[0]", is(step.getStepImpl().getArguments().get(0))));

    }
}
