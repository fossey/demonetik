package fr.ensicaen.demonetik.domain;

import fr.ensicaen.demonetik.api.error.DemonetikException;
import fr.ensicaen.demonetik.dao.ActorDao;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.domain.transaction.TransactionService;
import fr.ensicaen.demonetik.model.Actor;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TransactionServiceTests {

    TransactionService service = new TransactionService();

    Scenario scenario;

    @Before
    public void setUp() {
        ActorDao.getInstance().peopling(() -> {

            List<Actor> actorList = new ArrayList<>();
            actorList.add(DummyElementFactory.createDummyActor("A1", "A1-IMG", "A1-DESC"));
            actorList.add(DummyElementFactory.createDummyActor("A2", "A2-IMG", "A2-DESC"));
            actorList.add(DummyElementFactory.createDummyActor("A3", "A3-IMG", "A3-DESC"));
            actorList.add(DummyElementFactory.createDummyActor("A4", "A4-IMG", "A4-DESC"));

            return actorList;
        });

        Step s1 = DummyElementFactory.createDummyStep(1, "A1", "msg", Step.State.HOLDER_IDENTIFICATION, null);
        Step s2 = DummyElementFactory.createDummyStep(2, "A2", "msg", Step.State.REQUEST_AMOUNT, null);
        Step s3 = DummyElementFactory.createDummyStep(3, "A2", "msg", Step.State.REQUEST_AMOUNT, null);
        Step s4 = DummyElementFactory.createDummyStep(4, "A3", "msg", Step.State.REQUEST_PIN, null);

        scenario = DummyElementFactory.createDummyScenario("scenario1", "desc", s1, s2, s3, s4);
    }

    @Test
    public void testGoToTheNextStep() throws DemonetikException {

        Scenario s = DummyElementFactory.createDummyScenario();
        s.getSteps().add(DummyElementFactory.createDummyStep(5, "TPE", "message", Step.State.HOLDER_IDENTIFICATION, null));
        s.getSteps().add(DummyElementFactory.createDummyStep(10, "Bank", "TRANSACTION_SUCCEED", Step.State.TRANSACTION_SUCCEED, null));

        Transaction t = service.create(s);

        assertEquals(0, t.getCurrentStepValue());
        assertEquals(0, t.getCurrentStep().getSequence().intValue());
        assertEquals(Step.State.INITIALIZED, t.getCurrentStep().getState());


        service.goToTheNextStep(t.getId());

        assertEquals(1, t.getCurrentStepValue());
        assertEquals(5, t.getCurrentStep().getSequence().intValue());
        assertEquals(Step.State.HOLDER_IDENTIFICATION, t.getCurrentStep().getState());

        service.goToTheNextStep(t.getId());

        assertEquals(2, t.getCurrentStepValue());
        assertEquals(10, t.getCurrentStep().getSequence().intValue());
        assertEquals(Step.State.TRANSACTION_SUCCEED, t.getCurrentStep().getState());

    }

    @Test
    public void testGetActors() throws DemonetikException {

        Transaction t = service.create(scenario);

        List<Actor> actors = service.getActors(t.getId());

        assertEquals(3, actors.size());

    }

    @Test
    public void testGetProgression() throws DemonetikException {

        Transaction t = service.create(scenario);


        assertEquals(25.0, service.getProgression(t.getId()).getPercent());

        service.goToTheNextStep(t.getId());

        assertEquals(50.0, service.getProgression(t.getId()).getPercent());

        service.goToTheNextStep(t.getId());

        assertEquals(75.0, service.getProgression(t.getId()).getPercent());

        service.goToTheNextStep(t.getId());

        assertEquals(100.0, service.getProgression(t.getId()).getPercent());

        // Transaction blocked to last scenario step
        service.goToTheNextStep(t.getId());

        assertEquals(100.0, service.getProgression(t.getId()).getPercent());

        service.goToTheNextStep(t.getId());

        assertEquals(100.0, service.getProgression(t.getId()).getPercent());

    }

}
