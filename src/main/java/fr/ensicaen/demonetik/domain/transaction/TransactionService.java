package fr.ensicaen.demonetik.domain.transaction;

import fr.ensicaen.demonetik.api.error.DemonetikException;
import fr.ensicaen.demonetik.dao.TransactionDao;
import fr.ensicaen.demonetik.domain.scenario.ScenarioService;
import fr.ensicaen.demonetik.domain.steps.Context;
import fr.ensicaen.demonetik.domain.steps.IStep;
import fr.ensicaen.demonetik.domain.steps.StepDefaultImpl;
import fr.ensicaen.demonetik.domain.steps.StepsLoader;
import fr.ensicaen.demonetik.domain.websocket.SessionHandler;
import fr.ensicaen.demonetik.domain.websocket.WebSocket;
import fr.ensicaen.demonetik.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private static Logger logger = LoggerFactory.getLogger(TransactionService.class);
    private TransactionDao transactionDao = TransactionDao.getInstance();

    public Transaction create(Scenario scenario) throws DemonetikException {

        Transaction t = new Transaction(ScenarioService.cloneScenario(scenario), SessionHandler.getInstance());

        transactionDao.save(t);
        logger.info("Transaction " + t.getId() + " created");

        SessionHandler.getInstance().sendMessage(WebSocket.TRANSACTION_CREATED);

        return t;
    }

    public List<Transaction> getTransactions() {
        return transactionDao.getTransactions().stream().sorted(
                (t1, t2) -> {
                    if (t1.getDateObject().isAfter(t2.getDateObject())) {
                        return -1;
                    } else if (t1.getDateObject().isBefore(t2.getDateObject())) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
        ).collect(Collectors.toList());
    }

    public Transaction getTransaction(String transactionId) {
        return transactionDao.getTransactionById(transactionId);
    }

    public Step.State goToTheNextStep(String transactionId) {
        Transaction t = transactionDao.getTransactionById(transactionId);

        try {
            if (t.getCurrentStepValue() < t.getScenario().getSteps().size() - 1) {

                resolveStepImpl(t.getCurrentStep()).execute(new Context(t, new MessageParser()));
            }
        } catch (DemonetikException e) {
            logger.error("Step " + t.getCurrentStepValue() + " failed", e);
            return Step.State.TRANSACTION_FAILED;
        }

        logger.info("Transaction : " + t.getId() + " - State : " + t.getCurrentStep().getState());

        return t.getCurrentStep().getState();
    }

    public Step.State getState(String transactionId) {
        return transactionDao.getTransactionById(transactionId).getState();
    }

    public void putData(String transactionId, String key, String value) {
        transactionDao.getTransactionById(transactionId).putData(key, value);
        logger.info("Transaction : {} - <{}, {}> Data posted", transactionId, key, value);
    }

    public String getData(String transactionId, String key) {
        return transactionDao.getTransactionById(transactionId).getData(key);
    }

    public List<Actor> getActors(String transactionId) {
        return transactionDao.getTransactionById(transactionId).getActors();
    }

    public Progression getProgression(String transactionId) {
        return transactionDao.getTransactionById(transactionId).getProgression();
    }

    private IStep resolveStepImpl(Step currentStep) throws DemonetikException {

        StepImpl impl = currentStep.getStepImpl();

        if (impl != null) {

            logger.info(impl.getClassName());

            String[] args = new String[impl.getArguments().size()];
            args = impl.getArguments().toArray(args);
            return StepsLoader.loadStep(impl.getClassName(), args);
        } else {
            logger.debug("No step implementation defined. Default implementation applied.");
            return new StepDefaultImpl();
        }
    }

    public void deleteTransactions() {
        transactionDao.deleteAll();
    }
}
