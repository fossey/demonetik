package fr.ensicaen.demonetik.domain.steps;

import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.model.Step;
import org.apache.commons.lang.Validate;
import org.slf4j.LoggerFactory;

import java.util.List;

@SuppressWarnings("unsed")
public class Authorization extends StepDefaultImpl {

    private Float limit;
    private String variableName;

    /**
     * Define an authorization
     *
     * @param args First argument is name of variable which contains transaction's amount, second one is the limit.
     */
    public Authorization(String... args) {
        logger = LoggerFactory.getLogger(Authorization.class);
        logger.info("Authorization step instantiated");

        Validate.isTrue(args.length == 2);

        limit = Float.valueOf(args[0]);
        variableName = args[1];

        logger.info("Limit : {}", limit);
        logger.info("Variable Name : {}", variableName);
    }

    @Override
    public void execute(Context context) {

        Transaction t = context.getTransaction();

        Float amount = Float.valueOf(context.getTransaction().getData(variableName));

        if (amount.compareTo(limit) >= 0) {

            logger.info("Transaction amount is above the limit.");
            logger.info("Scenario is forking...");

            List<Step> fork = t.getCurrentStep().getScenarioFork();
            List<Step> newScenario = t.getScenario().getSteps().subList(0, t.getCurrentStepValue() + 1); // toIndex must be inclusive
            newScenario.addAll(fork);

            t.getScenario().setSteps(newScenario);
        }

        super.execute(context);
    }
}
