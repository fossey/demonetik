# Demonetik
---------

[![pipeline status](https://gitlab.com/fossey/Demonetik/badges/master/pipeline.svg)](https://gitlab.com/fossey/Demonetik/commits/master)
[![coverage report](https://gitlab.com/fossey/Demonetik/badges/master/coverage.svg)](https://gitlab.com/fossey/Demonetik/commits/master)


## Prerequisites :

* Maven v3.5.0
* Java v1.8.0

## Technologies :

* Spring-Boot v1.5.7
* AngularJS v1.6.7

## Getting Started :

* Run server :

```shell
> git clone https://gitlab.com/fossey/Demonetik.git
> cd demonetik
> mvn spring-boot:run
```

* Go to web view :

[http://localhost:8080/demonetik](http://localhost:8080/demonetik)

* Retrieve Android application

[https://gitlab.ecole.ensicaen.fr/sebille/Demonetik_TPE](https://gitlab.ecole.ensicaen.fr/sebille/Demonetik_TPE)

## Documentation :

### How does it works ?

```mermaid
sequenceDiagram

participant WS as Web Service
participant WC as Web Client 
participant A as Android
  
Note left of WS: Server initialization
activate WS
WS->>WS:WebService.loadActors();  
WS->>WS:WebService.loadScenarios();  
deactivate WS

Note right of A: List scenarios
A->>+WS: GET /scenarios  
WS-->>-A: 200: { scenarios: ... }
  
Note right of A: Load a specific one
A->>+WS: GET /scenarios/{scenarioId}/load  
WS->>WC: Websocket {TRANSACTION CREATED}
WS-->>-A: 200: { transactionId: "...." }  
Loop Every steps
A->>+WS: GET /transactions/{transactionId}  
WS-->>-A: { transaction: { state: {...} } }
  
A->>A: changeViewFromState(state)  
Note over A: User interaction    
A->>WS: POST /transactions/{executionId}/data  

A->>+WS: PUT /transactions/{executionId}  
WS->>WC: sendMessage({transactionId})  
Note over WC:Display information
Note over WC:Update progress bar
WS-->>-A: new transaction state  

end
```
### Write scenario :

#### States :

| States                 | Description                           |
| ---------------------- | ------------------------------------- |
| INITIALIZED            | Transaction is instantiated           |
| REQUEST_AMOUNT         | Request amount to TPE                 |
| HOLDER_IDENTIFICATION  | Request card passing                  |
| REQUEST_PIN            | Request card pin                      |
| TRANSACTION_PROCESSING | Transaction is managed by webservice  |
| TRANSACTION_SUCCEED    | Transaction succeed                   |
| TRANSACTION_FAILED     | Transaction failed                    |

#### Variables :

Here you can find variables posted by Android application on web service :

| Name       | Description         |
| ---------- | ------------------- |
| amount     | Amount typed on TPE |
| pin        | pin encrypted       |
| cardNumber | Number of card      |
| cardId     | Tag NFC (?)         |
| holderName | Card holder's name  |

You can use this variables in messages like this `{{amount}}`.

#### Implements your own step :

Create a class and implement ``fr.ensicaen.demonetik.domain.steps.IStep`` interface.

If you want to use it specified your qualified class name in ``className`` of ``stepImpl`` attribute's object. And your arguments in ``arguments`` attributes.

_Example :_
```json
{
      "sequence": "6",
      "actor": "Bank",
      "message": "Demande d'authorisation de la transaction à la banque pour le montant : {{amount}}",
      "state": "TRANSACTION_PROCESSING",
      "stepImpl":{
        "className": "fr.ensicaen.demonetik.domain.steps.Authorization",
        "arguments":["200","amount"]
      }
}
```

### Fork scenario :

You can fork you scenario in using ``scenarioFork`` attribute. 

```mermaid
graph LR
A((1)) --> B((2))
B((2)) --> C((3))
C((3)) --> D((4))
D((4)) --> E((5))
E((5)) --> F((6))
E((5)) --> G((6))
G((6)) --> H((7))
```

_Example :_ 

```json
{
      "sequence": "5",
      "actor": "Bank",
      "message": "Demande d'authorisation de la transaction à la banque pour le montant : {{amount}}",
      "state": "TRANSACTION_PROCESSING",
      "stepImpl":{
        "className": "fr.ensicaen.demonetik.domain.steps.Authorization",
        "arguments":["200","amount"]
      },
      "scenarioFork":[
        {
          "sequence": "6",
          "actor": "TPE",
          "message": "Résultat de l'authorisation",
          "state": "TRANSACTION_FAILED"
        }
      ]
    }
```

### Wiki pages :

* [Tasks definition](https://gitlab.ecole.ensicaen.fr/fossey/Demonetik/wikis/tasks-definition)
* [Set up a ci pipeline for Gitlab](https://gitlab.ecole.ensicaen.fr/fossey/Demonetik/wikis/Set-up-a-CI-pipeline-with-Gitlab)

### Maven plugins : 
* [jsonschema2pojo](https://github.com/joelittlejohn/jsonschema2pojo/wiki/Getting-Started#the-maven-plugin)
* [springmvc-raml-plugin](https://github.com/phoenixnap/springmvc-raml-plugin/tree/master/springmvc-raml-plugin#usage-3---generating-springmvc-server-endpoints-from-a-raml-file)