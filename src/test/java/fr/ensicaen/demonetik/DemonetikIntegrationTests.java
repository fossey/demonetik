package fr.ensicaen.demonetik;

import fr.ensicaen.demonetik.model.LoadingResponse;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.model.TransactionData;
import fr.ensicaen.demonetik.util.PathFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemonetikIntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Test scenario
     * 1. Return scenarios. Each scenario has an id
     * 2. Initialize a transaction from specified scenario. Return a transaction id
     * 3. For each steps :
     * - Check if transaction state returned is valid
     * - If state is REQUEST_AMOUNT post data <montant, 100>
     * - Go to the next step
     * <p>
     * <p/>
     * 1. GET /scenarios
     * 2. GET /scenarios/{scenarioId}/load
     * ----- loop ----- [ for each step ]
     * 3. GET /transaction/{transactionId}/state
     * <p>
     * ----- if REQUEST_AMOUNT -----
     * 3.1 POST /transaction/{transactionId}/data (key,value) (montant, 100)
     * 3.2 GET  /transaction/{transactionId}/data/{key} (value) (100)
     * ----- end if ------
     * <p>
     * 4. PUT /transaction/{transactionId}
     * ----- end loop -----
     */
    @Test
    public void testSimpleHappyPath() {

        ResponseEntity<Scenario[]> getScenariosResponse = restTemplate.getForEntity(PathFactory.getScenarios(), Scenario[].class);

        assertTrue(getScenariosResponse.getStatusCode().is2xxSuccessful());
        assertTrue(getScenariosResponse.hasBody());
        Scenario[] scenarios = getScenariosResponse.getBody();

        if (scenarios.length > 0) {

            Scenario main = scenarios[0];

            // Test scenario loading
            ResponseEntity<LoadingResponse> loadEntityResponse =
                    restTemplate.getForEntity(PathFactory.getLoadScenario(main.getId()), LoadingResponse.class);

            assertTrue(loadEntityResponse.getStatusCode().is2xxSuccessful());
            assertTrue(loadEntityResponse.hasBody());

            String transactionId = loadEntityResponse.getBody().getTransactionId();

            //Test each scenario step
            HttpEntity<Void> entity = new HttpEntity(Void.class);
            for (int i = 0; i < main.getSteps().size(); i++) {

                Step.State state = main.getSteps().get(i).getState();

                if (state.equals(Step.State.REQUEST_AMOUNT)) {
                    sendData(transactionId, "amount", "100");
                }

                ResponseEntity<String> responseEntity = restTemplate.exchange(PathFactory.putTransaction(transactionId), HttpMethod.PUT, entity, String.class);
                int nextI = i == main.getSteps().size() - 1 ? i : i + 1;
                testTransactionStepIsValid(responseEntity, main.getSteps().get(nextI).getState());
            }
        }
    }

    private void testTransactionStepIsValid(ResponseEntity<String> responseEntity, Step.State stateExpected) {

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
        assertTrue(responseEntity.hasBody());

        //If state return by web service is not at Step.State, fromValue method throw IllegalArgumentException
        Step.State stateTested = Step.State.fromValue(responseEntity.getBody());

        if (stateExpected != null) {
            assertEquals(stateExpected, stateTested);
        }
    }

    private void sendData(String transactionId, String key, String value) {
        TransactionData data = new TransactionData();
        data.setKey(key);
        data.setValue(value);

        ResponseEntity<Void> responseEntity = restTemplate.postForEntity(PathFactory.postTransactionData(transactionId), data, Void.class);
        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
    }
}
