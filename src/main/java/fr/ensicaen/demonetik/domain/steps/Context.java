package fr.ensicaen.demonetik.domain.steps;

import fr.ensicaen.demonetik.domain.transaction.IMessageParser;
import fr.ensicaen.demonetik.domain.transaction.Transaction;

public class Context {

    private Transaction transaction;
    private IMessageParser iMessageParser;

    public Context(Transaction transaction, IMessageParser iMessageParser) {
        this.transaction = transaction;
        this.iMessageParser = iMessageParser;
    }

    public Transaction getTransaction() {
        return transaction;
    }


    public IMessageParser getMessageParser() {
        return iMessageParser;
    }
}
