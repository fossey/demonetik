package fr.ensicaen.demonetik.domain.scenario;

import fr.ensicaen.demonetik.model.Step;

public interface IReceiver {

    void sendStep(String transactionIdFrom, Step step);

}
