package fr.ensicaen.demonetik.domain.transaction;

import java.util.Map;

public interface IMessageParser {

    String parseMessage(Map<String, String> sharedMemory, String message);
}
