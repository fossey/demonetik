package fr.ensicaen.demonetik.domain;

import fr.ensicaen.demonetik.domain.transaction.IMessageParser;
import fr.ensicaen.demonetik.domain.transaction.MessageParser;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;

public class IMessageParserTests {

    private IMessageParser parser;

    @Before
    public void setUp() {
        parser = new MessageParser();
    }

    @Test
    public void testGoodMessages() {

        HashMap<String, String> values = new HashMap<>();
        String msg = "OK";

        assertEquals("OK", parser.parseMessage(values, msg));

        values.put("amount", "15");
        msg = "Amount is {{amount}}€";

        assertEquals("Amount is 15€", parser.parseMessage(values, msg));

        msg = "Amount is {{amount}}€ {{amount}}€ {{amount}}€";
        assertEquals("Amount is 15€ 15€ 15€", parser.parseMessage(values, msg));

        values.put("balance","100");

        msg = "Amount is {{amount}}€ and my balance is {{balance}}€";
        assertEquals("Amount is 15€ and my balance is 100€", parser.parseMessage(values, msg));

        msg = "A dummy variable is {{dummy}} ";
        assertEquals(msg, parser.parseMessage(values, msg));

    }
}
