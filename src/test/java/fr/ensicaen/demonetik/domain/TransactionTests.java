package fr.ensicaen.demonetik.domain;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static junit.framework.TestCase.assertEquals;

public class TransactionTests {

    private static Logger logger = LoggerFactory.getLogger(TransactionTests.class);

    @Test
    public void testPutTransactionData() {

        Transaction t = new Transaction(DummyElementFactory.createDummyScenario(), (id, o) -> {
        });

        String amount = "15€";
        t.putData("amount", amount);

        assertEquals(amount, t.getData("amount"));

    }

    @Test(expected = NotFoundException.class)
    public void testTransactionDataIsNotFound() {
        Transaction t = new Transaction(DummyElementFactory.createDummyScenario(), (id, o) -> {
        });
        t.putData("amount", "15€");
        t.getData("balance");
    }
}
