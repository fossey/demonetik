package fr.ensicaen.demonetik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemonetikApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemonetikApplication.class, args);
	}
}
