package fr.ensicaen.demonetik.domain.steps;

import fr.ensicaen.demonetik.domain.transaction.IMessageParser;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import fr.ensicaen.demonetik.model.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StepDefaultImpl implements IStep {

    Logger logger = LoggerFactory.getLogger(StepDefaultImpl.class);

    @Override
    public void execute(Context context) {

        Transaction transaction = context.getTransaction();
        IMessageParser iMessageParser = context.getMessageParser();

        transaction.incrementCurrentStep();

        Step current = transaction.getCurrentStep();

        String rawStepMessage = current.getMessage();
        String parsedStepMessage = iMessageParser.parseMessage(transaction.getSharedMemory(), rawStepMessage);

        current.setMessage(parsedStepMessage);

        logger.info("Send \"{}\" to {}", current.getMessage(), current.getActor());
        transaction.sendStep(current);
        transaction.getPastSteps().addLast(current);

    }
}
