package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.domain.Provider;
import fr.ensicaen.demonetik.model.Scenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScenarioDao {

    private static Logger logger = LoggerFactory.getLogger(ScenarioDao.class);

    private static ScenarioDao instance;
    private static HashMap<String, Scenario> scenarios = new HashMap<>();


    private ScenarioDao() {}

    public static ScenarioDao getInstance() {
        if (instance == null) {
            instance = new ScenarioDao();
        }

        return instance;
    }

    public void peopling(Provider<Scenario> scenarioProvider){
        scenarioProvider.provideElements().forEach(
                scenario -> scenarios.put(scenario.getId(), scenario)
        );
    }

    public List<Scenario> getScenarios() {
        return new ArrayList<>(scenarios.values());
    }

    public Scenario getScenarioById(String id) {
        Scenario s = scenarios.get(id);

        if (s == null) {
            String errorMessage = "Scenario " + id + " not found";
            logger.error(errorMessage);
            throw new NotFoundException(errorMessage);
        }

        return s;
    }

}
