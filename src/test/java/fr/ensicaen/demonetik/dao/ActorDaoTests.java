package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.model.Actor;
import fr.ensicaen.demonetik.util.DummyElementFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ActorDaoTests {

    @Before
    public void setUp(){
        ActorDao.getActors().clear();
    }

    @After
    public void cleanUp(){
        ActorDao.getActors().clear();
    }

    @Test
    public void testInstanceCreation() {

        Actor a1 = DummyElementFactory
                .createDummyActor("A1", "http://example.com/actor-a1.png", "Actor 1");
        Actor a2 = DummyElementFactory
                .createDummyActor("A2", "http://example.com/actor-a2.png", "Actor 2");

        List<Actor> actorList = Arrays.asList(a1, a2);

        ActorDao dao = ActorDao.getInstance();

        dao.peopling(() -> actorList);

        assertEquals(actorList.size(), dao.getActors().size());
        assertTrue(dao.getActors().containsKey(a1.getName()));
        assertTrue(dao.getActors().containsKey(a2.getName()));

    }
}
