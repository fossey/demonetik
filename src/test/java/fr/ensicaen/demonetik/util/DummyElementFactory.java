package fr.ensicaen.demonetik.util;

import edu.emory.mathcs.backport.java.util.Arrays;
import fr.ensicaen.demonetik.model.Actor;
import fr.ensicaen.demonetik.model.Scenario;
import fr.ensicaen.demonetik.model.Step;
import fr.ensicaen.demonetik.model.StepImpl;

public class DummyElementFactory {

    public static Scenario createDummyScenario() {

        Step step = createDummyStep(0, "TPE", "Message 1", Step.State.INITIALIZED, null);
        return createDummyScenario("Scenario Test 1", "Scenario Test 1 - Description", step);

    }

    public static Scenario createDummyScenario(String name, String description, Step... steps) {

        Scenario scenario = new Scenario();
        scenario.setName(name);
        scenario.setDescription(description);

        scenario.getSteps().addAll(Arrays.asList(steps));

        java.util.Arrays.stream(steps)
                .map(Step::getActor)
                .distinct()
                .forEach(actor -> scenario.getActors().add(actor));

        return scenario;
    }

    public static Actor createDummyActor(String name, String image, String description) {

        Actor a = new Actor();
        a.setName(name);
        a.setImage(image);
        a.setDescription(description);

        return a;
    }

    public static Step createDummyStep(int sequence, String actor, String message, Step.State state, StepImpl impl) {

        Step step = new Step();
        step.setSequence(sequence);
        step.setActor(actor);
        step.setMessage(message);
        step.setState(state);
        step.setStepImpl(impl);

        return step;
    }

    //createDummyStepImpl("fr.some.Class",new String[]{"arg1"})
    public static StepImpl createDummyStepImpl(String className, String[] args) {

        StepImpl impl = new StepImpl();
        impl.setClassName(className);
        impl.setArguments(Arrays.asList(args));

        return impl;
    }

}
