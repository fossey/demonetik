package fr.ensicaen.demonetik.domain.actor;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ensicaen.demonetik.domain.Provider;
import fr.ensicaen.demonetik.model.Actor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ActorLoader implements Provider<Actor> {

    private static Logger logger = LoggerFactory.getLogger(ActorLoader.class);

    @Override
    public List<Actor> provideElements() {

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<Actor> actors = new ArrayList<>();

        try {

            File folder = new ClassPathResource("actors").getFile();

            File[] files = folder.listFiles(
                    (File dir, String name) -> name.endsWith("json")
            );

            for (int i = 0; i < files.length; i++) {
                Actor a = objectMapper.readValue(files[i], Actor.class);
                logger.info("[Actor] " + a.getName() + " loaded");
                actors.add(a);
            }

        } catch (IOException e) {
            logger.error("Actor loading error ", e);
        }

        return actors;
    }
}
