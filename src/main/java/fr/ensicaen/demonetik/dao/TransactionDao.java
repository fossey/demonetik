package fr.ensicaen.demonetik.dao;

import fr.ensicaen.demonetik.api.error.NotFoundException;
import fr.ensicaen.demonetik.domain.transaction.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransactionDao {

    private static Logger logger = LoggerFactory.getLogger(TransactionDao.class);

    private static TransactionDao instance;
    private HashMap<String, Transaction> transactions = new HashMap<>();

    private TransactionDao() {
    }

    public static TransactionDao getInstance() {
        if (instance == null) {
            instance = new TransactionDao();
        }

        return instance;
    }

    public List<Transaction> getTransactions(){
        return new ArrayList<>(transactions.values());
    }

    public void save(Transaction transaction) {
        transactions.put(transaction.getId(), transaction);
    }

    public Transaction getTransactionById(String transactionId) {

        Transaction t = transactions.get(transactionId);

        if (t == null) {
            String errorMessage = "Transaction " + transactionId + " not found";
            logger.error(errorMessage);
            throw new NotFoundException(errorMessage);
        }

        return t;
    }

    public void deleteAll(){
        logger.info("Delete {} transactions",transactions.size());
        transactions.clear();
    }

}
